#include "stringdop.h"

StringDop::StringDop()
{
}

StringDop::~StringDop()
{
}

// Разделить строку по тексту
std::vector<std::string> StringDop::splitStr(const std::string& str, const std::string& spl)
{
    std::regex rdelim{ spl };

    std::vector<std::string> ret{ std::sregex_token_iterator(str.begin(), str.end(), rdelim, -1), std::sregex_token_iterator() };

    // if (str == "BTCUSDT")
    // {
    //     std::cout << str << "; " << spl << "; " << ret.size() << "; " << ret[0] << std::endl;
    // }

    if (str.size() >= spl.size())
    {
        std::string symb;
        std::copy(str.begin() + (str.size() - spl.size()), str.end(), back_inserter(symb));
        if (symb == spl)
        {
            ret.push_back("");
        }
    }

    return ret;
}

// Верхний регистр
std::string StringDop::str_toupper(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) { return std::toupper(c); }  // correct
    );
    return s;
}

// Нижний регистр
std::string StringDop::str_tolower(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) { return std::tolower(c); }  // correct
    );
    return s;
}

std::size_t StringDop::replace_all(std::string& inout, std::string_view what, std::string_view with)
{
    std::size_t count{};
    for (std::string::size_type pos{}; inout.npos != (pos = inout.find(what.data(), pos, what.length())); pos += with.length(), ++count)
    {
        inout.replace(pos, what.length(), with.data(), with.length());
    }
    return count;
}

std::string StringDop::from_double(double number, int dig, std::string separ)
{
    std::string numbStr;
    if (number < 0)
    {
        numbStr += "-";
        number = number * (-1);
    }

    unsigned long long rNumb = static_cast<long long>(number);

    unsigned long long rNumbDig = number * (pow(10, dig));
    rNumbDig = rNumbDig - (rNumb * pow(10, dig));

    std::string numbStrRound = std::to_string(rNumb);

    if (numbStrRound.size() > 3 && separ != "")
    {
        std::string newStr;
        for (size_t i = 0; i < numbStrRound.size(); i++)
        {
            newStr += numbStrRound[numbStrRound.size() - (1 + i)];
            if ((i + 1) % 3 == 0 && i < numbStrRound.size() - 1)
            {
                newStr += separ;
            }
        }
        std::reverse(newStr.begin(), newStr.end());
        numbStrRound = newStr;
    }

    std::string numbStrDig = std::to_string(rNumbDig);

    size_t digN = static_cast<size_t>(dig) - numbStrDig.size();

    std::string nulls = "";
    if (digN > 0)
    {
        nulls += std::to_string(static_cast<unsigned long long>(pow(10, digN)));
        nulls.erase(nulls.begin());
    }
    nulls += numbStrDig;

    reverse(nulls.begin(), nulls.end());

    unsigned long long nnn = std::stoull(nulls);
    nulls = std::to_string(nnn);
    reverse(nulls.begin(), nulls.end());

    numbStr += numbStrRound;
    if (rNumbDig > 0)
    {
        numbStr += "." + nulls;
    }

    return numbStr;
}

std::string StringDop::wstring_to_utf8(const std::wstring& str)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
    return myconv.to_bytes(str);
}

std::wstring StringDop::utf8_to_wstring(const std::string& str)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
    return myconv.from_bytes(str);
}
