#ifndef WXJSON_H
#define WXJSON_H

#include "json.hpp"
#include <string>
#include <iostream>

using Json = nlohmann::json;

class wxJson
{
public:
    wxJson();
    ~wxJson();

    static Json parse(std::string text);
    static std::string toString(Json json);
    static double toDouble(Json json);
    static long double toLDouble(Json json);
    static unsigned long long toUULong(Json json);
    static long long toLLong(Json json);
    static unsigned long toULong(Json json);
    static long toLong(Json json);
    static int toInt(Json json);
    static bool toBool(Json json);

private:
    Json json;
};

#endif