#include "wxjson.h"

wxJson::wxJson()
{
}

wxJson::~wxJson()
{
}

Json wxJson::parse(std::string text)
{
    Json json;
    if (text == "")
    {
        return Json::parse("{}");
    }
    else
    {
        try
        {
            // parsing input with a syntax error
            json = Json::parse(text);
        }
        catch (Json::parse_error& e)
        {
            // output exception information
            std::cout << "message: " << e.what() << '\n' << "exception id: " << e.id << '\n' << "byte position of error: " << e.byte << std::endl;
            std::cout << "Text parse: " << text << std::endl;
        }
    }

    return json;
}

std::string wxJson::toString(Json json)
{
    if (json.is_string())
        return json.get<std::string>();
    return "";
}

double wxJson::toDouble(Json json)
{
    if (json.is_number())
        return json.get<double>();
    else if (json.is_string())
        return std::stod(json.get<std::string>());

    return 0;
}

long double wxJson::toLDouble(Json json)
{
    if (json.is_number())
        return json.get<long double>();
    else if (json.is_string())
        return std::stold(json.get<std::string>());

    return 0;
}

unsigned long long wxJson::toUULong(Json json)
{
    if (json.is_number())
        return json.get<unsigned long long>();
    else if (json.is_string())
        return std::stoull(json.get<std::string>());

    return 0;
}

long long wxJson::toLLong(Json json)
{
    if (json.is_number())
        return json.get<long long>();
    else if (json.is_string())
        return std::stoull(json.get<std::string>());

    return 0;
}

unsigned long wxJson::toULong(Json json)
{
    if (json.is_number())
        return json.get<unsigned long>();
    else if (json.is_string())
        return std::stoul(json.get<std::string>());

    return 0;
}

long wxJson::toLong(Json json)
{
    if (json.is_number())
        return json.get<long>();
    else if (json.is_string())
        return std::stol(json.get<std::string>());

    return 0;
}

int wxJson::toInt(Json json)
{
    if (json.is_number())
        return json.get<int>();
    else if (json.is_string())
        return std::stoi(json.get<std::string>());

    return 0;
}

bool wxJson::toBool(Json json)
{
    if (json.is_boolean())
        return json.get<bool>();

    return false;
}
