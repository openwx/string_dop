#ifndef STRINGDOP_H
#define STRINGDOP_H

#include <codecvt>
#include <string>
#include <locale>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <string_view>
#include <cmath>
#include <regex>

class StringDop
{
public:
    StringDop();
    ~StringDop();

    static std::vector<std::string> splitStr(const std::string& str, const std::string& spl);  // Разделить строку по тексту
    static std::string str_toupper(std::string s);                                             // Верхний регистр
    static std::string str_tolower(std::string s);                                             // Нижний регистр

    static std::size_t replace_all(std::string& inout, std::string_view what, std::string_view with);  // Заменить текст

    static std::string from_double(double number, int dig, std::string separ = "");

    // convert wstring to UTF-8 string
    static std::string wstring_to_utf8(const std::wstring& str);

    // convert UTF-8 string to wstring
    static std::wstring utf8_to_wstring(const std::string& str);

private:
};

#endif