# StringDop

`#include <stringdop.h>`

### Разделить строку по тексту

```
std::vector<std::string> spString = StringDop::splitStr("Hello World!!", "W");
if (!spString.empty())
{
    for (std::string text : spString)
    {
        std::cout << text << std::endl;
    }
}
```

Вывод

```
Hello World!!
```

### Текст в верхний регистр

`std::cout << StringDop::str_toupper("hello") << std::endl;`

Вывод

`HELLO`

### Текст в нижний регистр

`std::cout << StringDop::str_tolower("HELLO") << std::endl;`

Вывод

`hello`

# wxJson

Дополнение к [Json cpp](https://github.com/nlohmann/json)

`#include <wxjson.h>`

Зоздание переменной;

`Json jsonData;`

Строку в Json

`wxJson::parse(std::string text);`

Получение значений

```
wxJson::toString(Json json);
wxJson::toDouble(Json json);
wxJson::toUULong(Json json);
wxJson::toULong(Json json);
wxJson::toLong(Json json);
wxJson::toBool(Json json);
```
